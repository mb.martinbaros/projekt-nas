import 'package:flutter/material.dart';
import "dart:convert";
import 'dart:async';
import 'package:http/http.dart' as http;
import "package:intl/intl.dart";

Map _data;
List _features;

void main() async {
  _data = await getQuakes();
  _features = _data["features"];
  print(_data["features"][0]["properties"]);

  runApp(MaterialApp(
    title: "Quakes",
    home: Quakes(),
  ));
}

class Quakes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Quakes"),
        backgroundColor: Colors.red,
        centerTitle: true,
      ),
      body: Center(
        child: ListView.builder(
            itemCount: _features.length,
            padding: const EdgeInsets.all(15.0),
            itemBuilder: (BuildContext context, int position) {
              if (position.isOdd) return Divider();
              final index = position ~/ 2;
              var format = DateFormat.yMMMMd("en_US").add_jm();
              //var dateString = format.format(date)
              var date = format.format(DateTime.fromMicrosecondsSinceEpoch(_features[index]["properties"]["time"]*1000, isUtc: true));

              return ListTile(
                title: Text(
                  "At $date",
                  style: TextStyle(
                      fontSize: 19.0,
                      color: Colors.orange,
                      fontWeight: FontWeight.w500),
                ),
                subtitle: Text(
                  "${_features[index]["properties"]["place"]}",
                  style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.italic),
                ),
                leading: CircleAvatar(
                  backgroundColor: Colors.green,
                  child: Text(
                    "${_features[index]["properties"]["mag"]}",
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.white,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                onTap: () {_showAlertMessage(context, "${_features[index]["properties"]["title"]}");},
              );
            }),
      ),
    );
  }

  void _showAlertMessage(BuildContext context, String message) {
    var alert = AlertDialog(
      title: Text("Quakes"),
      content: Text(message),
      actions: <Widget>[
        FlatButton(onPressed: () => Navigator.pop(context), child: Text("OK"))
      ],
    );
    showDialog(context: context, builder: (context) => alert);
  }
}

Future<Map> getQuakes() async {
  String apiUrl =
      "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson";
  http.Response responce = await http.get(apiUrl);

  return json.decode(responce.body);
}
