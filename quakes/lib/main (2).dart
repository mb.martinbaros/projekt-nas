import 'package:flutter/material.dart';
import "dart:async";
import "dart:convert";
import 'package:http/http.dart' as http;

void main() async {
  List _data = await getJson();
  print(_data[0]["title"]);
  String _body ;

  for (int i = 0; i < _data.length; i++) {
    print(_data[i]["title"]);
  }

  _body = _data[0]["body"];

  runApp(MaterialApp(
    home: Scaffold(
      appBar: AppBar(
        title: Text("Json parse"),
        centerTitle: true,
        backgroundColor: Colors.orangeAccent,
      ),
      body: Center(
        child: ListView.builder(
            itemCount: _data.length,
            padding: const EdgeInsets.all(15),
            itemBuilder: (BuildContext context, int position) {

              return Column(
                children: <Widget>[
                  Divider(height: 5.5),
                  ListTile(
                    title: Text(
                      "${_data[position]["title"]}",
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                    subtitle: Text(
                      "${_data[position]["body"]}",
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.grey,
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                    leading: CircleAvatar(
                      backgroundColor: Colors.greenAccent,
                      child: Text(
                        "${_data[position]["body"][0]}".toUpperCase(),
                        style: TextStyle(
                            fontSize: 15.4, color: Colors.orangeAccent),
                      ),
                    ),
                    onTap:() => _showTapMessage(context, _data[position]["body"]),
                  )
                ],
              );
            }),
      ),
    ),
  ));
}



void _showTapMessage(BuildContext context, String message) {
  var alert = AlertDialog(
    title: Text("My App"),
    content: Text(message),
    actions: <Widget>[FlatButton(
        onPressed: (){Navigator.pop(context);},
        child: Text("OK"))],
  );
  showDialog(context: context, builder: (context) => alert);

}

Future<List> getJson() async {
  String apiUrl = "https://jsonplaceholder.typicode.com/posts";
  http.Response response = await http.get(apiUrl);
  return json.decode(response.body);
}
