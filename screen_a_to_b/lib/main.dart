import 'package:flutter/material.dart';
import 'dart:async';

void main() {
  runApp(MaterialApp(
    title: "Screen",
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var _nameFieldController = TextEditingController();

  Future _goToNextScreen (BuildContext context) async{
    var results = await Navigator.of(context).push(
     MaterialPageRoute<dynamic>(
         builder: (BuildContext context){
       return NextScreen( name: _nameFieldController.text);
       })
    );
    if results.containsKey("info")
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("First screen"),
        centerTitle: true,
        backgroundColor: Colors.pinkAccent,
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: TextField(
              controller: _nameFieldController,
              decoration: InputDecoration(labelText: "Enter Your Name"),
            ),
          ),
          ListTile(
            title: RaisedButton(
                child: Text("Send to next screen"),
                onPressed:() async {
                  var router = await _goToNextScreen(context);
                }

                          /*(BuildContext context) => NextScreen(
                            name: "${_nameFieldController.text}",
                          ));
                  Navigator.of(context).push(router);
                }),*/
          ),
          )
        ],
      ),
    );
  }
}

class NextScreen extends StatefulWidget {
  final String name;

  const NextScreen({Key key, this.name}) : super(key: key);

  @override
  _NextScreenState createState() => _NextScreenState();
}

class _NextScreenState extends State<NextScreen> {
  var _backTextFieldController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Screen"),
        backgroundColor: Colors.pinkAccent,
        centerTitle: true,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text("${widget.name}"),
            ),
            ListTile(
              title: TextField(
                controller: _backTextFieldController,
              ),
            ),
            ListTile(
              title: FlatButton(
                  onPressed: (){
                    Navigator.pop(context, {
                      "info": _backTextFieldController.text
                    });
                  },
                  child: Text("Send data back")
              ),
            )
          ],
        ),
      ),
    );
  }
}
