
class Location{
  num lat, lng;
  Location(this.lat, this.lng);
  
}
class ElevatedLocation extends Location{
  num elevation;

  ElevatedLocation(num lat, num lng,this.elevation) : super(lat, lng);

}



main(List<String> arguments) {
   var elevated = ElevatedLocation(23.32, 234.98,90);

   print("location= ${elevated.lat}, ${elevated.lng}, ${elevated.elevation}");
}

