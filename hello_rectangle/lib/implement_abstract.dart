abstract class Animal{
  void breathe();
  void makeNoise(){
    print("Making animal noises!");
  }
}

abstract class IsFunny{
  void makePeopleLaugh();
}

class TVShow implements IsFunny{
  String name;
  @override
  void makePeopleLaugh() {
    print("TVShow is funny and makes people laugh!");
  }
}

class Comedian extends Person implements IsFunny{
  Comedian(String name, String nationality) : super(name,nationality);

  @override
  void makePeopleLaugh() {
    print("Comedian making people laugh")
  }
}




class Person implements Animal{
  String name, nationality;


  Person(this.name, this.nationality);

  @override
  void breathe() {
    print("Breathing person best person");
  }
  @override
  void makeNoise() {
    print("Person Noiseee!");
  }
  @override
  String toString() => "$name $nationality";
}



main(List<String> arguments) {
   var jenny = new Person("Jenny", "Jamaican");
       print(jenny);
}

