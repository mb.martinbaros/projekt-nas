import "package:flutter/material.dart";
import 'package:task_02_category_widget/category.dart';

// TODO: Pass this information into your custom [Category] widget
const _categoryName = 'Cake';
const _categoryIcon = Icons.cake;
const _categoryColor = Colors.green;

/// The function that is called when main.dart is run.
void main() {
  runApp(UnitConverterApp());
}

/// This widget is the root of our application.
/// Currently, we just show one widget in our app.
class UnitConverterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Unit Converter',
      home: Scaffold(
        backgroundColor: Colors.green[100],
        body: Center(
          // TODO: Determine what properties you'll need to pass into the widget
          child: Category(),
        ),
      ),
    );
  }
}

Widget build (BuildContext context){
  assert(debugCheckHasMaterial(context));
  return Material(
    color: Colors.transparent,
    child: Container(
      height: 100,
      child: InkWell(
        borderRadius: _borderRadius,
        highlightColor: color[50],
        splashColor: color[100],
        onTap: (){
          print("Dotyk")
        },
        child: Padding(padding: EdgeInsets.all(8.0),
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right:8.0),
            child: iconLocation!=null ? Icon(Icon.cake),
          )
        ],)

        ),),
      ),
    );
  }


void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: AppBar(
        title: Text("Hello Rectangle"),
      ),
      body: HelloRectangle(),
    ),
  ));
}
