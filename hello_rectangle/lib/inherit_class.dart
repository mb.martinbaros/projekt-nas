class Person {
  String name, lastname, nationality;
  int age;

  void showName() {
    print(this.name);
  }
  void sayHello(){
    print("Hello");
  }
  void showNationality(){
    print("American");
  }
}

class Bonni extends Person {
  String profession;

  void showProfesion() => print(profession);
  @override
  void showNationality() {
    // TODO: implement showNationality
    print("Korean/American");
  }
}

class Paulo extends Person {
  bool playGuitar;
  @override
  void sayHello() {
    // TODO: implement sayHello
    print("Ola");
      }
  @override
  void showNationality() {
    // TODO: implement showNationality
    print("Mozambican");
  }
}

main(List<String> arguments) {
  var bonni = new Bonni();
  bonni.name = "Bonni";
  bonni.profession = "Balerina";
  bonni.sayHello();
  bonni.showNationality();
  bonni.showName();
  bonni.showProfesion();

  var paulo = new Paulo();
  paulo.name = "Paulo";
  paulo.age = 45;
  paulo.sayHello();
  paulo.showNationality();
  paulo.playGuitar = true;
  print(paulo.age);
  paulo.showName();
}

