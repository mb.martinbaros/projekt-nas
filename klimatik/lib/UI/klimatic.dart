import 'package:flutter/material.dart';
import 'dart:async';
import '../util/utils.dart' as util;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:math' as math;
import 'package:intl/intl.dart';

Map _data;
List _features = [];

class Klimatic extends StatefulWidget {
  @override
  _KlimaticState createState() => _KlimaticState();
}

class _KlimaticState extends State<Klimatic> {

  String _cityEntered;
  var image = "fewclouds.png";



  Future _goToNextScreen(BuildContext context) async {
    var results = await Navigator.of(context)
        .push(MaterialPageRoute<dynamic>(builder: (BuildContext context) {
      return new ChangeCity();
    }));

    if (results != null && results.containsKey("enter")){
      _cityEntered = results["enter"];
    }
  }
  Future _goToNextScreen2(BuildContext context) async {
    _data = await getQuakes();
    _features = _data ["features"];
    var druhe = await Navigator.of(context)
        .push(MaterialPageRoute<dynamic>(builder: (BuildContext context) {
      return new Quakes();
    }));


  }

  void showStuff() async {
    Map data = await getWeather(util.appId, util.defaultCity);
    print(data.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Klimatic"),
        backgroundColor: Colors.red,
        centerTitle: true,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                _goToNextScreen(context);
              }),
          IconButton(
              icon: Icon(Icons.adjust),
              onPressed: () {
                _goToNextScreen2(context);
              })
        ],
      ),
      body: Stack(
        children: <Widget>[
          Center(
            child: Image.asset(
              "images/umbrella.jpg",
              width: 490.0,
              height: 1200,
              fit: BoxFit.fill,
            ),
          ),
          Container(
            alignment: Alignment.topRight,
            margin: const EdgeInsets.fromLTRB(0.0, 11.0, 20.9, 0.0),
            child: Text("${_cityEntered == null ? util.defaultCity : _cityEntered}", style: cityStyle()),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(40.0, 220.0, 0.9, 0.0),
            child: Image.asset(
              "images/$image",
              width: 100.0,
              height: 120.0,
            color: Colors.white,),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(15.0, 330.0, 0.0, 0.0),
            child: updateTempWidget(_cityEntered),
          )
        ],
      ),
    );
  }

  Future<Map> getWeather(String appId, String city) async {
    String apiUrl =
        "https://api.openweathermap.org/data/2.5/weather?q=$city&units=metric&appid=${util.appId}";
    http.Response response = await http.get(apiUrl);
    return json.decode(response.body);
  }

  Widget updateTempWidget(String city) {
    return FutureBuilder(
      future: getWeather(util.appId, city == null ? util.defaultCity : city),
      builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {
        if (snapshot.hasData) {
          Map content = snapshot.data;
          /*String porovnavacka = content["weather"]["main"].toString();
          void changeImage(){
            switch () {
              case "Clouds" : {
                image = "lightrain.png";}
              break;

              default : {
                image = "fewclouds.png";
              }
            }
          }
          changeImage();*/

          return Container(
            child: Column(
              children: <Widget>[
                ListTile(
                  title: Text(
                    "${content["main"]["temp"].toString()} °C",
                    style: tempStyle(),
                  ),
                  subtitle: ListTile(
                    title: Text(
                      "Humidity: ${content["main"]["humidity"].toString()} %\n"
                          "Min: ${content["main"]["temp_min"].toString()} °C\n"
                          "Max: ${content["main"]["temp_max"].toString()} °C"
                      ,style: extraData(),
                    ),
                  ),
                )
              ],
            ),
          );
        } else {
          return Container(
            child: Text("Loading ...."),
          );
        }
      },
    );
  }
}

class ChangeCity extends StatelessWidget {
  var _cityFieldController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text("Change City"),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          Center(
            child: Image.asset(
              "images/snowyy.jpg",
              width: 490.0,
              height: 1200.0,
              fit: BoxFit.fill,
            ),
          ),
          ListView(
            children: <Widget>[
              ListTile(
                title: TextField(
                  decoration: InputDecoration(
                    hintText: "Enter City",
                  ),
                  controller: _cityFieldController,
                  keyboardType: TextInputType.text,
                ),
              ),
              ListTile(
                title: FlatButton(
                    onPressed: (){
                      Navigator.pop(context, {
                        "enter" : _cityFieldController.text
                      });
                    },
                    color: Colors.red,
                    textColor: Colors.white70,
                    child: Text("Get Weather")
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

TextStyle cityStyle() {
  return TextStyle(
    fontSize: 26.0,
    color: Colors.white,
    fontStyle: FontStyle.italic,
  );
}

TextStyle tempStyle() {
  return TextStyle(
      fontSize: 50.0,
      color: Colors.white,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500);
}

TextStyle extraData() {
  return TextStyle(
      fontSize: 16.0,
      color: Colors.white70,
      fontStyle: FontStyle.normal,
      );
}




Future<Map> getQuakes() async {
  String urlApi = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson";  http.Response response = await http.get(urlApi);
  return json.decode(response.body);
}


class Quakes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Quakes"),
        centerTitle: true,
        backgroundColor: Colors.lightGreen,
      ),
      body: Center(
        child: ListView.builder(
            itemCount: _features.length,
            padding: EdgeInsets.all(4.0),
            itemBuilder: (BuildContext context, int position) {
              if (position.isOdd)return Divider();
              final index = position ~/2;
              var format = new DateFormat.yMMMMd("en_US")..add_jm();
              var date = format.format(DateTime.fromMillisecondsSinceEpoch(_features[index]["properties"]["time"],isUtc: true));
              var radius = _features[index]["properties"]["mag"];
              return new ListTile(
                title: new Text(
                  " $date",
                  style: TextStyle(
                    fontSize: 18.5,
                    color: Colors.lightGreen,
                    fontWeight: FontWeight.w500,
                  ),
                  textAlign: TextAlign.right,),
                subtitle: Text("Place: ${_features[index]["properties"]["place"]}",
                  style: TextStyle(
                    fontSize: 14.5,
                    color: Colors.black38,
                    fontWeight: FontWeight.w400,),
                  textAlign: TextAlign.right,
                ),
                leading: CircleAvatar(
                  radius: ((radius/7 )+ 1) *20.0,
                  backgroundColor: Color((math.Random().nextDouble() * 0xFFFFFF).toInt() << 0).withOpacity(1.0),
                  child: Text(
                    "${_features[index]["properties"]["mag"]}",
                    style: TextStyle(
                      fontSize: 16.5,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),),
                ),
                onTap:(){
                  _showAlertMessage (context,"${_features[index]["properties"]["title"]}");
                } ,
              );
            }),
      ),
    );
  }
}

void _showAlertMessage (BuildContext context,String message){
  var alert = AlertDialog(
    backgroundColor: Colors.lightGreen,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
    title: Text("Quakes"),
    content: Text(message),
    actions: <Widget>[
      FlatButton(onPressed: (){Navigator.pop(context);},
          child:Text("OK",style: TextStyle(color: Colors.deepOrangeAccent),))
    ],
  );
  showDialog(context: context, builder : (context)=> alert);
}
