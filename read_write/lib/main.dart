import "package:flutter/material.dart";
import "package:path_provider/path_provider.dart";
import "dart:async";
import "dart:io";


void main(){
  runApp(MaterialApp(
    title: "IO",
    home: Home(),
  ));
}


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }


  Future<String> get _localPath async{
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }
  Future<File> get _localFile async{
    final path = await _localPath;
    return File("$path/data.txt");
  }
  Future<File> writeData(String message) async{
    final file = await _localFile;

    //write file
    return file.writeAsString("$message");

  }
  Future<String> readData() async{
    try{
      final file = await _localFile;
          //read
      String data = await file.readAsString();
      return data;
    }catch(e){
      return "Nothing saved yettttt!";
    }
  }

}
